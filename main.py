from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse
from modules.composition import Composition

class Layout(Widget):
    pass

class PaintApp(App):
    def build(self):
        return Layout()
    
if __name__ == '__main__':
    PaintApp().run()