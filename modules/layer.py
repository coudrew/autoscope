from kivy.uix.widget import Widget
from kivy.graphics import Color, Point, Line

class Layer(Widget):

    def __init__(self, **kwargs):
        super(Layer, self).__init__(**kwargs)
        self.color = '#000000ff' if not(bool('colorprop' in kwargs)) else kwargs['colorprop']
        self.point_size = 2.0 if not(bool('pointsize' in kwargs)) else kwargs['pointsize']
        self.instructions = []

    def add_instruction(self, instruction):
        self.instructions.append(instruction)

    def get_instructions(self):
        return self.instructions
    
    def draw(self, points):
        with self.canvas:
            Color(self.color)
            Line(points=(points), width=(self.point_size))
