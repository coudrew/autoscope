from kivy.uix.widget import Widget
from modules.layer import Layer

class Composition(Widget):

    def __init__(self, **kwargs):
        super(Composition, self).__init__(**kwargs)
        self.layers = [Layer(**kwargs)]
        self.selected_layer = 0
        self.points = []
        self.drawing = False
        self.add_widget(self.layers[0])

    def add_layer(self, **kwargs):
        self.layers.append(Layer(**kwargs))
        self.selected_layer = len(self.layers) - 1
        self.add_widget(self.layers[self.selected_layer])

    def set_selected_layer(self, layer:int):
        self.selected_layer = layer

    def add_point(self, point):
        self.points.append(point[0])
        self.points.append(point[1])

    def on_touch_down(self, touch):
        print(touch.pos)
        self.drawing = True
        self.add_point(touch.pos)
        self.layers[self.selected_layer].draw(self.points)

    def on_touch_move(self, touch):
        if self.drawing:
            self.add_point(touch.pos)
            self.layers[self.selected_layer].draw(self.points)

    def on_touch_up(self, touch):
        self.layers[self.selected_layer].add_instruction(self.points)
        self.drawing = False
        self.points = []